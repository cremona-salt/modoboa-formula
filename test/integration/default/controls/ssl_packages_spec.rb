# frozen_string_literal: true

platform_finger = system.platform[:finger].split('.').first.to_s

if platform_finger == 'Ubuntu-18'
  control 'modoboa ssl packages certbot repo managed' do
    describe apt('ppa:certbot/certbot') do
      it { should exist }
      it { should be_enabled }
    end
  end
end

control 'modoba ssl packages certbot package' do
  title 'should be installed'

  describe package('certbot') do
    it { should be_installed }
  end
end
