# frozen_string_literal: true

os_family = system.platform[:family]
if os_family == 'debian'
  control 'modoboa amavis config node config' do
    title 'should match desired lines'

    describe file('/etc/amavis/conf.d/05-node_id') do
      it { should be_file }
      it { should be_owned_by 'root' }
      it { should be_grouped_into 'root' }
      its('mode') { should cmp '0644' }
      its('content') { should include('#  Your changes will be overwritten.') }
      its('content') { should include('$myhostname = "mail.example.com";') }
    end
  end
  control 'modoboa amavis config content filter mode config' do
    title 'should match desired lines'

    describe file('/etc/amavis/conf.d/15-content_filter_mode') do
      it { should be_file }
      it { should be_owned_by 'root' }
      it { should be_grouped_into 'root' }
      its('mode') { should cmp '0644' }
      its('content') { should include('#  Your changes will be overwritten.') }
    end
  end
  control 'modoboa amavis config user config' do
    title 'should match desired lines'

    describe file('/etc/amavis/conf.d/50-user') do
      it { should be_file }
      it { should be_owned_by 'root' }
      it { should be_grouped_into 'root' }
      its('mode') { should cmp '0644' }
      its('content') { should include('#  Your changes will be overwritten.') }
      its('content') { should include('$max_servers = 1') }
      its('content') do
        should include(
          "@lookup_sql_dsn = ( [ 'DBI:Pg:database=amavis;host=127.0.0.1', "\
          "'amavis', 'secret' ]);"
        )
      end
    end
  end
end
