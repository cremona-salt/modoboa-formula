# frozen_string_literal: true

control 'modoboa amavis postgres database' do
  title 'should be created'

  describe command('sudo --user postgres '\
                   'psql --list --quiet --tuples-only | '\
                   'cut --delimiter \| --fields 1 | '\
                   'grep --word-regexp amavis | '\
                   'wc --lines') do
    its('stdout') { should eq "1\n" }
  end
end

control 'modoboa amavis postgres database' do
  title 'should be owned by amavis'

  describe command('sudo --user postgres '\
                   'psql --list --quiet --tuples-only | '\
                   'grep --extended-regexp "^\s+amavis" | '\
                   'cut --delimiter \| --fields 2 | '\
                   'grep --word-regexp amavis | '\
                   'wc --lines') do
    its('stdout') { should eq "1\n" }
  end
end

control 'modoboa amavis postgres user' do
  title 'should be created'

  describe command('sudo --user postgres '\
                   'psql --tuples-only --no-align '\
                   '--command '\
                   '"SELECT 1 FROM pg_roles WHERE rolname=\'amavis\'"') do
    its('stdout') { should eq "1\n" }
  end
end

finger = system.platform[:finger].capitalize
control 'modoboa amavis postgres file' do
  title 'should be created'

  describe file('/tmp/postgres-amavis.sql') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0644' }
    its('content') { should include "-- Amavis postgres 2.11.X for #{finger}" }
  end
end

control 'modoboa amavis postgres amavis msgs table' do
  title 'should be created'

  describe command('sudo --user postgres '\
                   'psql --tuples-only --no-align --dbname amavis '\
                   '--command '\
                   '"SELECT 1 FROM pg_catalog.pg_tables WHERE tablename=\'msgs\'"') do
    its('stdout') { should eq "1\n" }
  end
end
