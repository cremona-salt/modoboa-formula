# frozen_string_literal: true

platform_family = system.platform[:family]

packages = case platform_family
           when 'redhat' then
             %w[amavis arj cabextract lz4 lrzip lzop p7zip unar unzoo]
           else
             %w[libdbi-perl amavisd-new arc
                arj cabextract liblz4-tool lrzip lzop p7zip-full rpm2cpio
                unrar-free]
           end

packages.each do |pkg|
  control "modoboa amavis packages #{pkg} package" do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
