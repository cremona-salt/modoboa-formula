# frozen_string_literal: true

packages = %w[sudo wget]
packages.each do |pkg|
  control "modoboa base packages #{pkg} package" do
    title 'should be installed'

    describe package(pkg) do
      it { should be_installed }
    end
  end
end
