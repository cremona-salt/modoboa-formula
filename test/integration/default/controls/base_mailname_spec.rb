# frozen_string_literal: true

control 'modoboa base mailname file' do
  title 'should match desired lines'

  describe file('/etc/mailname') do
    it { should be_file }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0644' }
    its('content') { should include('mail.example.com') }
  end
end
