# -*- coding: utf-8 -*-
# vim: ft=sls

include:
  - .packages
  - .generate
  - .cron
