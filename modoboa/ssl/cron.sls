# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import modoboa with context %}
{%- set sls_ssl_generate = tplroot ~ '.ssl.generate' %}

include:
  - {{ sls_ssl_generate }}

{% set certtype = modoboa.certificate.type %}
{% set generate = modoboa.certificate.generate %}

{% if certtype == 'letsencrypt' %}
{% if generate == true %}
{% set test_cert = modoboa.ssl.lets_encrypt.test_cert %}
{% set hostname = modoboa.hostname %}
modoboa-ssl-cron-renewal-job-managed:
  cron.present:
    - name: certbot renew --quiet --no-self-upgrade --force-renewal{% if test_cert == true %} --test-cert{% endif %}
    - user: root
    - minute: 0
    - hour: '*/12'
    - require:
      - sls: {{ sls_ssl_generate }}

modoboa-ssl-cron-renewal-config-set-to-nginx:
  file.replace:
    - name: /etc/letsencrypt/renewal/{{ hostname }}.conf
    - pattern: 'authenticator = standalone'
    - repl: 'authenticator = nginx'
{% endif %}
{% endif %}
