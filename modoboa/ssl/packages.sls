# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import modoboa with context %}

{%- set certtype = modoboa.certificate.type %}

{% if certtype == 'letsencrypt' %}
{% set osfinger = salt.grains.get('osfinger', None) %}
{% if osfinger == 'Ubuntu-18.04' %}
modoboa-ssl-packages-lets-encrypt-ppa-managed:
  pkgrepo.managed:
    - ppa: certbot/certbot
{% endif %}

modoboa-ssl-packages-lets-encrypt-pkgs-installed:
  pkg.installed:
    - pkgs: {{ modoboa.ssl.lets_encrypt.pkgs | yaml }}
    {% if osfinger == 'Ubuntu-18.04' %}
    - refresh: true
    - require:
      - modoboa-ssl-packages-lets-encrypt-ppa-managed
    {% endif %}
{% endif %}
