# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import modoboa with context %}
{%- set sls_ssl_packages = tplroot ~ '.ssl.packages' %}

include:
  - {{ sls_ssl_packages }}

{%- set certtype = modoboa.certificate.type %}
{%- set generate = modoboa.certificate.generate %}

{% if certtype == 'letsencrypt' %}
{% if generate == true %}
{% set hostname = modoboa.hostname %}
{% set check_cert_cmd = 'certbot renew --dry-run --cert-name' %}
{% set certbot_cmd = 'certbot certonly --non-interactive --standalone -d ' ~ hostname ~ ' --email ' ~ letsencrypt_email ~ ' --agree-tos' %}
{% set letsencrypt_email = modoboa.ssl.lets_encrypt.email %}
{% set test_cert = modoboa.ssl.lets_encrypt.test_cert %}
modoboa-ssl-generate-lets-encrypt-certificate-created:
  cmd.run:
    - runas: root
    - unless: {{ check_cert_cmd }}
    - name {{ certbot_cmd }}{% if test_cert == true %} --test-cert{% endif %}
    - require:
      - sls: {{ sls_ssl_packages }}
{% endif %}
{% endif %}
