# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import modoboa with context %}

modoboa-base-install-pkg-installed:
  pkg.installed:
    - pkgs: {{ modoboa.base.pkgs | yaml }}
