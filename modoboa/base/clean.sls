# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_config_clean = tplroot ~ '.config.clean' %}
{%- from tplroot ~ "/map.jinja" import modoboa with context %}

include:
  - {{ sls_config_clean }}

modoboa-base-clean-pkg-removed:
  pkg.removed:
    - pkgs: {{ modoboa.base.pkg | yaml }}
