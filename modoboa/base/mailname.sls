
# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import modoboa with context %}

modoboa-base-mailname-file-managed:
  file.managed:
    - name: /etc/mailname
    - user: root
    - group: root
    - mode: 644
    - contents:
      - {{ modoboa.hostname }}
