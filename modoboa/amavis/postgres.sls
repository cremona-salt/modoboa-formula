# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import modoboa with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

modoboa-amavis-postgres-database-user-managed:
  postgres_user.present:
    - name: {{ modoboa.amavis.dbuser }}
    - password: {{ modoboa.amavis.dbpasswd }}

modoboa-amavis-postgres-database-db-created:
  postgres_database.present:
    - name: {{ modoboa.amavis.dbname }}
    - owner: {{ modoboa.amavis.dbuser }}
    - require:
      - modoboa-amavis-postgres-database-user-managed

modoboa-amavis-postgres-sql-file-managed:
  file.managed:
    - name: /tmp/postgres-amavis.sql
    - source: {{ files_switch(['amavis_postgres.sql'],
                              lookup='modoboa-amavis-postgres-sql-file-managed',
                              default_files_switch=["osfinger"],
                              use_subpath=True
                 )
              }}
    - mode: 644
    - user: root
    - group: root

modoboa-amavis-postgres-sql-executed:
  cmd.run:
    - name: psql --dbname {{ modoboa.amavis.dbname }} < /tmp/postgres-amavis.sql
    - runas: postgres
    - require:
      - modoboa-amavis-postgres-database-db-created
      - modoboa-amavis-postgres-sql-file-managed
