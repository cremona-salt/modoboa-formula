# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import modoboa with context %}

{% set os_family = salt.grains.get('os_family', None) %}

modoboa-amavis-install-pkgs-installed:
  pkg.installed:
    - pkgs: {{ modoboa.amavis.pkgs | yaml }}
