# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.amavis.install' %}
{%- from tplroot ~ "/map.jinja" import modoboa with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{% set os_family = salt.grains.get('os_family', None) %}

include:
  - {{ sls_package_install }}

{% if os_family == 'Debian' %}
modoboa-amavis-config-node-id-managed:
  file.managed:
    - name: /etc/amavis/conf.d/05-node_id
    - source: {{ files_switch(['node_id.tmpl'],
                              lookup='modoboa-amavis-config-node-id-managed',
                              use_subpath=True
                 )
              }}
    - mode: 644
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_package_install }}
    - context:
        modoboa: {{ modoboa | json }}

modoboa-amavis-config-content-filter-mode-managed:
  file.managed:
    - name: /etc/amavis/conf.d/15-content_filter_mode
    - source: {{ files_switch(['content_filter_mode.tmpl'],
                              lookup='modoboa-amavis-config-content-filter-mode-managed',
                              use_subpath=True
                 )
              }}
    - mode: 644
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_package_install }}
    - context:
        modoboa: {{ modoboa | json }}

modoboa-amavis-config-user-managed:
  file.managed:
    - name: /etc/amavis/conf.d/50-user
    - source: {{ files_switch(['user.tmpl'],
                              lookup='modoboa-amavis-config-user-managed',
                              use_subpath=True
                 )
              }}
    - mode: 644
    - user: root
    - group: root
    - makedirs: True
    - template: jinja
    - require:
      - sls: {{ sls_package_install }}
    - context:
        modoboa: {{ modoboa | json }}
{% endif %}
