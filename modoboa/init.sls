# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import modoboa with context %}

include:
  - .base
  - .ssl
  {% if modoboa.amavis.enabled == true %}
  - .amavis
  {% endif %}
