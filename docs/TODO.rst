TODO
====

- Check config file
- Install sudo ✓
- Install wget ✓
- ssl_backend.generate_cert()
   - lets encrypt ✓
   - self-signed
- scripts.install(amavis)
   - Write hostname to ``/etc/mailname`` ✓
   - Install: ✓
      - Deb: ✓
         - libdbi-perl ✓
         - amavisd-new ✓
         - arc ✓
         - arj ✓
         - cabextract ✓
         - liblz4-tool ✓
         - lrzip ✓
         - lzop ✓
         - p7zip-full ✓
         - rpm2cpio ✓
         - unrar-free ✓
         - libdbd-<DB_DRIVER>-perl
      - RPM:
         - amavis ✓
         - arj ✓
         - cabextract ✓
         - lz4 ✓
         - lrzip ✓
         - lzop ✓
         - p7zip ✓
         - unar ✓
         - unzoo ✓
         - perl-DBD-<DB_DRIVER>
   - Setup db:
      - Create db user
      - Create db
      - Load sql file for amavis
   - Install config files
      - deb:
         - conf.d/05-node_id
         - conf.d/15-content_filter_mode
         - conf.d/50-user
      - rpm:
         - amavisd.conf

- if amavis:
   - scripts.install(spamassassin)
      - Install spamassassin, pyzor
      - Setup db:
         - Create db user
         - Create db
         - Load sql file for spamassassin
      - Install config files
         - v310.pre
         - local.cf
      - Run pyzor cmd
      - Install razor
      - if deb:
         - enable cron job
   - scripts.install(clamav)
      - Install
         - Deb:
            - Install clamav-daemon
         - rpm:
            - Install clamav
            - Install clamav-update
            - Install clamav-server
            - Install clamav-server-systemd
      - Config files
         - if rpm:
            - sysconfig/clamd.amavisd
            - tmpfiles.d/clamd.amavisd.conf
      - if deb:
         - Add clamav user to amavis group
         - Enable ``AllowSupplementaryGroups`` in ``/etc/clamav/clamd.conf``
      - if rpm:
         - Comment ``Example`` in ``/etc/freshclam.conf``
         - Ensure correct wanted by in
           ``/usr/lib/systemd/system/clamd@.service``
      - Stop freshclam to allow manual download


- scripts.install(modoboa)
   - install packages:
      - deb:
         - build-essential
         - python3-dev
         - libxml2-dev
         - libxslt-dev
         - libjpeg-dev
         - librrd-dev
         - rrdtool
         - libffi-dev
         - cron
         - libssl-dev
         - redis-server
         - supervisor
      - rpm:
         - gcc
         - gcc-c++
         - python3-devel
         - libxml2-devel
         - libxslt-devel
         - libjpeg-turbo-devel
         - rrdtool-devel
         - rrdtool
         - libffi-devel
         - supervisor
         - redis
   - create modoboa user
- scripts.install(automx)
- scripts.install(radicale)
- scripts.install(uswgi)
- scripts.install(nginx)
- scripts.install(opendkim)
- scripts.install(postfix)
- scripts.install(dovecot)
- restart cron
